import * as React from "react";
import * as ReactDOM from "react-dom";
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import {appReducer} from "./repository/AppReducer";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import App from "./view/App";

const middleware = [ thunk ];
    const store = createStore(appReducer, composeWithDevTools(
    applyMiddleware(...middleware),
));


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
  document.getElementById("root") as HTMLElement
);
