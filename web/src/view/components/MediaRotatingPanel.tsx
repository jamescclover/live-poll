import {buildUrl} from "../../repository/MediaFunctions";
import * as React from "react";
import {FunctionComponent, useState} from "react";
import AutoFitImage from "react-image-autofit-frame";
import {makeStyles} from "@material-ui/styles";
import {MediaReference} from "../../repository/domain/ApiTypes";
import {useDispatch} from "react-redux";
import {Button} from "@material-ui/core";
import {ArrowBackIos, ArrowForwardIos} from "@material-ui/icons";
import {decrementWithWrap, incrementWithWrap} from "../../repository/MiscFunctions";
import showMediaOnStageAction from "../../repository/action/ShowMediaOnStageAction";

interface Props {
    media: MediaReference[],
    frameWidth: number,
    frameHeight: number
}

const MediaRotatingPanel: FunctionComponent<Props> = ({media, frameWidth, frameHeight}) => {
    const dispatch = useDispatch();
    const classes = useStyles({});
    const [currentMediaIndex, setCurrentMediaIndex] = useState(0);

    if(!media || media.length === 0) {
        let frameStyle = {
            width: `${frameWidth}px`,
            height: `${frameHeight}px`,
            backgroundColor: 'light grey'
        };
        return <div style={frameStyle}/>
    }
    else {
        const frames = media.map(ref =>
            <AutoFitImage frameWidth="{frameWidth}" frameHeight="{frameHeight}" imgSrc={buildUrl(ref)} imgSize="contain"/>);
        const increment = () => setCurrentMediaIndex(incrementWithWrap(currentMediaIndex, media.length));
        const decrement = () => setCurrentMediaIndex(decrementWithWrap(currentMediaIndex, media.length));
        const sendToStage = () => dispatch(showMediaOnStageAction.build(media[currentMediaIndex].id));

        // TODO need to add purposes here (maybe not here, but in a similar component)
        return (
            <div className={classes.vertical}>
                <div className={classes.horizontal}>
                    {frames.length > 1 && <Button className={classes.scrollButton} onClick={decrement}><ArrowBackIos/></Button> }
                    <AutoFitImage frameWidth={frameWidth}
                                  frameHeight={frameHeight}
                                  imgSrc={buildUrl(media[currentMediaIndex])}
                                  imgSize="contain"/>
                    {frames.length > 1 && <Button className={classes.scrollButton} onClick={increment}><ArrowForwardIos/></Button> }
                </div>
                <Button onClick={sendToStage}>Send to stage</Button>
            </div>
        );
    }
};

const useStyles = makeStyles(theme => ({
    horizontal: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
    },
    vertical: {
        display: 'flex',
        flexDirection: 'column',
    },
    condition: {
        fontStyle: 'italic'
    },
    scrollButton: {
        height: '100%',
        width: '25px'
    }
}));

export default MediaRotatingPanel;