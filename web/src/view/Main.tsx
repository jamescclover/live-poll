import * as React from "react";
import {FunctionComponent} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/styles";
import {AppState} from "../repository/state/AppState";
import {Choice, VoteTotal} from "../repository/domain/ApiTypes";
import Choices from "./Choices";
import Totals from "./Totals";
import AlreadyVoted from "./AlreadyVoted";
import Polls from "./Polls";
import {Button} from "@material-ui/core";
import selectPollAction from "../repository/action/SelectPollAction";
import setCurrentPage from "../repository/action/SetCurrentPage";
import {choicePage, totalsPage} from "../repository/action/ActionFunctions";

/**
 * Main screen for the voting app - decides what screen to render depending on the state of the user
 */
const Main: FunctionComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles({});

    const selectedPoll: string = useSelector((state: AppState) => state.pollId);
    const page: string = useSelector((state: AppState) => state.currentPage);
    const currentVote: string = useSelector((state: AppState) => state.voteChoice);
    const selectPage = (page: string) => dispatch(setCurrentPage.build(page));

    const header = (
        <div className={classes.pageChoices}>
            <Button color="primary" className={classes.pageButton} onClick={() => selectPage(choicePage)}>
                Choices
            </Button>
            <Button color="primary" className={classes.pageButton} onClick={() => selectPage(totalsPage)}>
                Totals
            </Button>
        </div>
    );

    let content = null;

    if(!selectedPoll) return <Polls/>;

    if(page === totalsPage) content = <Totals/>;
    else if(currentVote) content = <AlreadyVoted/>;
    else content =  <Choices/>;

    return (
        <div className={classes.vertical}>
            {header}
            {content}
        </div>
    )

};

const useStyles = makeStyles({
    vertical: {
        display: 'flex',
        flexDirection: 'column',
    },
    pageChoices: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
        width: '100%'
    },
    center: {
        alignContent: 'center'
    },
    pageButton: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },

});

export default Main;