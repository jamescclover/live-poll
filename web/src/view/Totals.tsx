import * as React from "react";
import {FunctionComponent, useEffect, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/styles";
import resetVoteTotalsAction from "../repository/action/ResetVoteTotalsAction";
import {Fab} from "@material-ui/core";
import DeleteSweep from "@material-ui/icons/DeleteSweep";
import {AppState} from "../repository/state/AppState";
import loadVotingState from "../repository/action/LoadVotingState";

/**
 * Shows the total vote totals in a graph
 */
const Totals: FunctionComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles({});

    const totals = useSelector((state: AppState) => state.totals);
    const resetTotals = () => dispatch(resetVoteTotalsAction.build());
    useInterval(() => dispatch(loadVotingState.build()), 10000);

    return (
        <div>
            {JSON.stringify(totals)}

            <div className={classes.fab} >
                <Fab onClick={resetTotals}><DeleteSweep/></Fab>
            </div>
        </div>
    );

};

const useStyles = makeStyles({

    fab: {
        position: 'absolute',
        bottom: 20,
        right: 20,
        margin: 2
    },

});

/**
 * Custom hook by Dan Abramov - https://overreacted.io/making-setinterval-declarative-with-react-hooks/
 * @param callback the call back to use
 * @param delay the time to wait between invocations, in milliseconds
 */
function useInterval(callback, delay) {
    const savedCallback = useRef();

    // Remember the latest callback.
    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    // Set up the interval.
    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}

export default Totals;