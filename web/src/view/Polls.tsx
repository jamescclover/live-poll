import * as React from "react";
import {FunctionComponent} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/styles";
import {AppState} from "../repository/state/AppState";
import selectPollAction from "../repository/action/SelectPollAction";
import {Button} from "@material-ui/core";

/**
 * Shows the available polls
 */
const Polls: FunctionComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles({});

    const pollIds: string[] = useSelector((state: AppState) => state.availablePolls);
    const selectPoll = (pollId: string) => dispatch(selectPollAction.build(pollId));

    return (
        <div className={classes.vertical}>
            {pollIds.map(id => (
                <Button color="primary" onClick={() => selectPoll(id)} key={id}>
                    {id}
                </Button>
            ))}
        </div>
    );

};

const useStyles = makeStyles({
    vertical: {
        display: 'flex',
        flexDirection: 'column',
    },
});

export default Polls;