import * as React from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import {withStyles} from "@material-ui/core/styles";
import {Theme, WithStyles} from "@material-ui/core";
import createStyles from "@material-ui/core/styles/createStyles";
import {AppState} from "../repository/state/AppState";
import {connect} from "react-redux";
import {Choice, VoteTotal} from "../repository/domain/ApiTypes";
import Main from "./Main";
import loadAllPolls from "../repository/action/LoadAllPolls";

interface DataProps {
    isLoaded: boolean,
    choices: Choice[];
    totals: VoteTotal[];
}

interface FunctionProps {
    loadInitialState: () => void;
}

interface Props extends DataProps, FunctionProps, WithStyles<typeof styles> {}

/**
 * The Live Poll app
 */
class App extends React.Component<Props> {

    public componentDidMount = () => {
        this.props.loadInitialState();
    };

    public render() {
        const { classes } = this.props;

        const loading = <span>Loading...</span>;
        const content = <Main/>;

        return (
            <React.Fragment>
                <CssBaseline/>
                <div className={classes.app}>
                    {this.props.isLoaded ? content : loading}
                </div>
            </React.Fragment>
    );
    }
}

const styles = ({ palette, spacing }: Theme) => createStyles({
    app: {
        display: 'flex',
        flexDirection: 'column',
        width: '100vw',
        height: '100vh',
        margin: 0
    },
});

const mapStateToProps = (state: AppState) : DataProps => ({
    isLoaded: state.isLoaded,
    choices: state.choices,
    totals: state.totals
});

const mapDispatchToProps = (dispatch) : FunctionProps => ({
    loadInitialState: () => { dispatch(loadAllPolls.build()) },
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));