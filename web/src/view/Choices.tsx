import * as React from "react";
import {FunctionComponent, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/styles";
import resetVoteTotalsAction from "../repository/action/ResetVoteTotalsAction";
import {AppBar, Fab, IconButton} from "@material-ui/core";
import DeleteSweep from "@material-ui/icons/DeleteSweep";
import ArrowForwardIos from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import {AppState} from "../repository/state/AppState";
import {TypedMap} from "../repository/domain/MapTypes.ds";
import {Choice} from "../repository/domain/ApiTypes";
import voteForAction from "../repository/action/VoteForAction";
import {decrementWithWrap, incrementWithWrap} from "../repository/MiscFunctions";
import Toolbar from '@material-ui/core/Toolbar';

/**
 * Shows a swipeable chooser and allows voting on it
 */
const Choices: FunctionComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles({});

    const choices: Choice[] = useSelector((state: AppState) => state.choices);
    const [currentIndex, setCurrentIndex] = useState(0);

    const voteForCurrent = () => dispatch(voteForAction.build(choices[currentIndex].id));
    const moveToPrevious = () => setCurrentIndex(decrementWithWrap(currentIndex, choices.length));
    const moveToNext = () => setCurrentIndex(incrementWithWrap(currentIndex, choices.length));

    return (
        <div className={classes.vertical}>

            <div className={classes.center}>
                <img className={classes.fit} src={`/public/${choices[currentIndex].imageUrl}`}/>
            </div>

            <AppBar position="fixed" color="primary" className={classes.appBar}>
                <Toolbar>
                    <IconButton onClick={moveToPrevious} edge="start" color="inherit">
                        <ArrowBackIos/>
                    </IconButton>

                    <div className={classes.grow} />
                    <Fab color="secondary" onClick={voteForCurrent}>Vote!</Fab>
                    <div className={classes.grow} />

                    <IconButton onClick={moveToNext} edge="end" color="inherit">
                        <ArrowForwardIos/>
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    );

};

const useStyles = makeStyles({
    vertical: {
        display: 'flex',
        flexDirection: 'column',
    },
    center: {
        alignContent: 'center'
    },
    grow: {
        flexGrow: 1,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
        height: '10vh'
    },
    fit: {
        width: '100vw',
        height: '90vh',
        objectFit: 'contain'
    },





});

export default Choices;