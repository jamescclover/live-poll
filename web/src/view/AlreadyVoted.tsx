import * as React from "react";
import {FunctionComponent} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/styles";
import {AppState} from "../repository/state/AppState";
import {Choice} from "../repository/domain/ApiTypes";

/**
 * Shows the single vote already cast
 */
const AlreadyVoted: FunctionComponent = () => {
    const dispatch = useDispatch();
    const classes = useStyles({});

    const choices: Choice[] = useSelector((state: AppState) => state.choices);
    const currentVote: string = useSelector((state: AppState) => state.voteChoice);
    const currentChoice: Choice = choices.find(c => c.id === currentVote);
    return (
        <div className={classes.vertical}>
            <div className={classes.center}>
                <img className={classes.fit} src={`/public/${currentChoice.imageUrl}`}/>
            </div>
        </div>
    );

};

const useStyles = makeStyles({
    vertical: {
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center'
    },
    center: {
        alignContent: 'center'
    },
    fit: {
        width: '100vw',
        height: '100vh',
        objectFit: 'contain'
    },

});

export default AlreadyVoted;