export interface TypedMap<T> { [key: string]: T; }

/**
 * Creates a new map, copying the original and updatoing a new member
 */
export const copyTypedMapWithUpdate = <T>(original: TypedMap<T>, updatedId: string, updatedValue: T) : TypedMap<T> => {
    const updated = {};
    Object.keys(original).forEach(id => updated[id] = original[id]);
    updated[updatedId] = updatedValue;

    return updated;
};