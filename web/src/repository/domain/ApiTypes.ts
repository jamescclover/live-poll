
export interface VoteTotal {
    id: string;
    total: number;
}

export interface Choice {
    id: string;
    name: string;
    imageUrl: string;
}

export interface ChoicesAndTotals {
    totals: VoteTotal[];
    choices: Choice[];
    voteChoice: string;
}