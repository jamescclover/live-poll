/**
 * Functions for interacting with the backend
 */
import {ChoicesAndTotals} from "./domain/ApiTypes";

class Repository {
    private jsonHeaders = {
        "Content-Type": "application/json"
    };

    private baseUrl: string = "/api/polls";
    private currentUrlParam: string = "current";
    private voteUrlParam: string = "vote-for";
    private resetUrlParam: string = "reset";

    /**
     * Gets all the current poll IDs
     */
    public loadPollList = () : Promise<string[]> =>
        fetch(`${this.baseUrl}`, {
            headers: this.jsonHeaders
        })
            .then(response => response.json());

    /**
     * Gets all the results of the voting and the choices, depending on the user's roles
     */
    public loadCurrent = (pollId: string) =>
        fetch(`${this.baseUrl}/${pollId}/${this.currentUrlParam}`, {
            headers: this.jsonHeaders
        })
            .then(response => response.json());

    /**
     * Clears all results
     */
    public clearResults = (pollId: string) =>
        fetch(`${this.baseUrl}/${pollId}/${this.resetUrlParam}`, {
            method: "POST",
            headers: this.jsonHeaders
        })
            .then((response) => response.json());


    /**
     * Registers this user's vote for the given item
     */
    public voteFor = (pollId: string, choiceId: string) : Promise<ChoicesAndTotals> =>
        fetch(`${this.baseUrl}/${pollId}/${this.voteUrlParam}/${choiceId}`, {
            method: "PUT",
            headers: this.jsonHeaders
        })
            .then((response) => response.json());

}

const repository = new Repository();
export default repository;
