import {Choice, VoteTotal} from "../domain/ApiTypes";
import {choicePage} from "../action/ActionFunctions";

/**
 * The Redux state of the application
 */
export interface AppState {
    isLoaded: boolean;
    availablePolls: string[];
    pollId: string;
    choices: Choice[];
    totals: VoteTotal[];
    voteChoice: string;

    currentPage: string;
}

export const initialState: AppState = {
    isLoaded: false,
    availablePolls: null,
    pollId: null,
    choices: null,
    totals: null,
    voteChoice: null,

    currentPage: choicePage
};

