/**
 * Increments the given number, wrapping to zero if needed
 */
export const incrementWithWrap = (current: number, size: number) => {
    let next = current + 1;
    if(next >= size) {
        next = 0
    }
    return next;
};

/**
 * Decrements the number, wrapping to the max value if needed
 */
export const decrementWithWrap = (current: number, size: number) => {
    let next = current - 1;
    if(next < 0) {
        next = size - 1;
    }
    return next;
};

