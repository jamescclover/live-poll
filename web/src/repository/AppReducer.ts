import {AppState, initialState} from "./state/AppState";
import resetVoteTotalsAction from "./action/ResetVoteTotalsAction";
import voteForAction from "./action/VoteForAction";
import loadVotingState from "./action/LoadVotingState";
import selectPollAction from "./action/SelectPollAction";
import loadAllPolls from "./action/LoadAllPolls";
import setCurrentPage from "./action/SetCurrentPage";

export interface Reducer<T> {
    type: string;
    reduce: (state: AppState, action: T) => AppState;
}

const reducers: Array<Reducer<any>> = [
    loadAllPolls,
    loadVotingState,
    selectPollAction,
    voteForAction,
    resetVoteTotalsAction,
    setCurrentPage
];

export const appReducer = (state: AppState = initialState, action: any) => {
    for (const reducer of reducers) {
        if(reducer.type === action.type) {
            return reducer.reduce(state, action);
        }
    }

    return state
};
