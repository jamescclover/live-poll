import repository from "../Repository";
import {AppState, initialState} from "../state/AppState";

interface ReduceParameters {
    type: string,
}

/**
 * Resets the vote totals
 */
export class ResetVoteTotalsAction {
    public type = 'ResetVoteTotalsAction';

    public build = () => (dispatch, getState) => {
        repository.clearResults()
            .then(dispatch({type: this.type}));
    };

    public reduce = (state: AppState, action: ReduceParameters): AppState => {
        return initialState;
    }

}

const resetVoteTotalsAction = new ResetVoteTotalsAction();
export default resetVoteTotalsAction;
