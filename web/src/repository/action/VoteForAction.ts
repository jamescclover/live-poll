import repository from "../Repository";
import {AppState, initialState} from "../state/AppState";
import {ChoicesAndTotals} from "../domain/ApiTypes";
import {getCurrentPoll} from "./ActionFunctions";

interface ReduceParameters {
    type: string,
    voteChoice: string
}

/**
 * Registers this user's vote for the given vote id
 */
export class VoteForAction {
    public type = 'VoteForAction';

    public build = (id: string) => (dispatch, getState) =>
        repository.voteFor(getCurrentPoll(getState()), id)
            .then((choicesAndTotals: ChoicesAndTotals) => dispatch({type: this.type, voteChoice: choicesAndTotals.voteChoice}));

    public reduce = (state: AppState, action: ReduceParameters): AppState => {
        return {
            ...state,
            voteChoice: action.voteChoice
        };
    }

}

const voteForAction = new VoteForAction();
export default voteForAction;
