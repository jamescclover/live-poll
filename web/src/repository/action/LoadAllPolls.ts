import repository from "../Repository";
import {AppState} from "../state/AppState";

interface ReduceParameters {
    type: string;
    pollIds: string[];
}

/**
 * Loads the initial state of the app
 */
export class LoadAllPolls {
    public type = 'LoadAllPolls';

    public build = () => (dispatch, getState) => {
        repository.loadPollList()
            .then((pollIds: string[]) => dispatch({
                type: this.type,
                pollIds: pollIds
            }));
    };

    public reduce = (state: AppState, action: ReduceParameters): AppState => (
        {
            ...state,
            isLoaded: true,
            availablePolls: action.pollIds
        }
    );

}

const loadAllPolls = new LoadAllPolls();
export default loadAllPolls;
