import {TypedMap} from "../domain/MapTypes.ds";
import {
    Choice,
    VoteTotal
} from "../domain/ApiTypes";
import {AppState} from "../state/AppState";

export const choicePage = "CHOICES";
export const totalsPage = "TOTALS";

/**
 * Returns the ID of the currently selected poll
 */
export const getCurrentPoll = (state: AppState) =>
    state.pollId;

/**
 * Unpacks arrays of choices into the state map
 */
export const unpackChoicesForState = (choices: Choice[]) : TypedMap<Choice> => {
    const targetChoiceMap: TypedMap<Choice> = {};

    choices.forEach(c => {
        targetChoiceMap[c.id] = c;
    });

    return targetChoiceMap
};

/**
 * Unpacks arrays of vote totals into the state map
 */
export const unpackTotalsForState = (totals: VoteTotal[]) : TypedMap<VoteTotal> => {
    const targetVoteCountMap: TypedMap<VoteTotal> = {};

    totals.forEach(c => {
        targetVoteCountMap[c.id] = c;
    });

    return targetVoteCountMap
};

/**
 * Unpacks arrays of vote totals into the state map
 */
export const unpackChoiceTotalsForState = (totals: VoteTotal[]) : TypedMap<VoteTotal> => {
    const targetVoteCountMap: TypedMap<VoteTotal> = {};

    totals.forEach(c => {
        targetVoteCountMap[c.id] = c;
    });

    return targetVoteCountMap
};


