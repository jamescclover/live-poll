import repository from "../Repository";
import {AppState, initialState} from "../state/AppState";
import {Choice, ChoicesAndTotals, VoteTotal} from "../domain/ApiTypes";
import {getCurrentPoll} from "./ActionFunctions";

interface ReduceParameters {
    type: string;
    pollId: string;
    choices: Choice[];
    totals: VoteTotal[];
    voteChoice: string;
}

/**
 * Selects the poll with the given ID
 */
export class SelectPollAction {
    public type = 'SelectPollAction';

    public build = (pollId: string) => (dispatch, getState) =>
        repository.loadCurrent(pollId)
            .then((choicesAndTotals: ChoicesAndTotals) => dispatch({
                type: this.type,
                pollId,
                choices: choicesAndTotals.choices,
                totals: choicesAndTotals.totals,
                voteChoice: choicesAndTotals.voteChoice
            }));

    public reduce = (state: AppState, action: ReduceParameters): AppState => (
        {
            ...state,
            pollId: action.pollId,
            choices: action.choices,
            totals: action.totals,
            voteChoice: action.voteChoice
        }
    );
}

const selectPollAction = new SelectPollAction();
export default selectPollAction;
