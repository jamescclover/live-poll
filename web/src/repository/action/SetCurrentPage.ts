import {AppState} from "../state/AppState";

interface ReduceParameters {
    type: string,
    page: string
}

/**
 * Sets the current choice being viewed to the given choice
 */
export class SetCurrentPage {
    public type = 'SetCurrentPage';


    public build = (page: string) : ReduceParameters => (
        {
            type: this.type,
            page
        }
    );

    public reduce = (state: AppState, action: ReduceParameters): AppState => (
        {
            ...state,
            currentPage: action.page
        }
    );

}

const setCurrentPage = new SetCurrentPage();
export default setCurrentPage;
