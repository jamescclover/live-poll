import repository from "../Repository";
import {AppState} from "../state/AppState";
import {Choice, ChoicesAndTotals, VoteTotal} from "../domain/ApiTypes";
import {getCurrentPoll} from "./ActionFunctions";

interface ReduceParameters {
    type: string;
    choices: Choice[];
    totals: VoteTotal[];
    voteChoice: string;
}

/**
 * Loads the initial state of the app
 */
export class LoadVotingState {
    public type = 'LoadVotingState';

    public build = () => (dispatch, getState) => {
        repository.loadCurrent(getCurrentPoll(getState()))
            .then((choicesAndTotals: ChoicesAndTotals) => dispatch({
                type: this.type,
                choices: choicesAndTotals.choices,
                totals: choicesAndTotals.totals,
                voteChoice: choicesAndTotals.voteChoice
            }));
    };

    public reduce = (state: AppState, action: ReduceParameters): AppState => (
        {
            ...state,
            isLoaded: true,
            choices: action.choices,
            totals: action.totals,
            voteChoice: action.voteChoice
        }
    );

}

const loadVotingState = new LoadVotingState();
export default loadVotingState;
