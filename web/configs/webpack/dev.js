// development config
const merge = require('webpack-merge');
const webpack = require('webpack');
const commonConfig = require('./common');

module.exports = merge(commonConfig, {
  mode: 'development',
  entry: './index.tsx', // the entry point of our app
  devServer: {
    hot: true, // enable HMR on the server
    progress: false,
    port: 3000,
    overlay: true,
    proxy: [{
      context: ['/api', '/auth', "/home"],
      target: 'http://localhost:8090',
      secure: false,
      changeOrigin: true
    }]
  },
  devtool: 'cheap-module-eval-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(), // enable HMR globally
  ],
});
