// const PATHS = require('./paths.config');

module.exports = {
    hot: true,
    progress: false,
    historyApiFallback: true,
    overlay: true,
    logLevel: 'silent',
    port: 3000,
    proxy: [{
        context: ['/api', '/auth', "/home"],
        target: 'http://localhost:8090',
        secure: false,
        changeOrigin: true
    }]

};

// contentBase: PATHS.build,
