// Imports
const path = require('path');
const webpack = require('webpack');

// Plugins
//const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const Notifier = require('node-notifier');

// Check for development mode
const IS_DEV = process.env.NODE_ENV === 'development';

// Configurations
const PATHS = require('./paths.config');
const stats = require('./stats.config');

// Webpack Base Configuration
const config = {
    entry: path.join(PATHS.src, 'index.tsx'),
    stats: stats,
    output: {
        path: PATHS.build
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', { loader: 'css-loader', options: { importLoaders: 1 } }],
            },
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'sass-loader',
                ],
            },
            {
                test: /\.(jpe?g|png|gif)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=img/[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optipng.optimizationLevel=7&gifsicle.interlaced=false',
                ],
            },
            // Load fonts and SVG images
            { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, use: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, use: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: "url-loader?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: "file-loader" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: "url-loader?limit=10000&mimetype=image/svg+xml"},

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    plugins: [
        new FriendlyErrorsWebpackPlugin({
            onErrors: (severity, errors) => {
                if (severity !== 'error') {
                    return;
                }
                const error = errors[0];
                Notifier.notify({
                    title: 'Webpack Error!',
                    message: severity + ': ' + error.name,
                    subtitle: error.file || ''
                    //icon: ICON
                });
            }
        }),
        // new Dotenv({
        //     path: path.join(PATHS.root, '.env'), // load this now instead of the ones in '.env'
        //     safe: false, // Load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
        //     systemvars: true, // Load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
        //     silent: false // Hide errors
        // }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.join(PATHS.src, 'index.html.ejs')
        })
    ]
};

module.exports = config;
