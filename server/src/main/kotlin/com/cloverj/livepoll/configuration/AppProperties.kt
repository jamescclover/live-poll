package com.cloverj.livepoll.configuration

import com.cloverj.livepoll.api.domain.Choice
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.PropertySources
import org.springframework.stereotype.Component
import java.util.*

/**
 * Application properties structure
 */
@ConfigurationProperties("livepoll")
class AppProperties {
    var security: SecurityProps? = null
    var pollConfigurations: List<String> = listOf()
}

class SecurityProps {
    var baseUri: String? = null
//    var overrideUser: String? = null
//    var startLoginUri: String? = null
//    var logoutUri: String? = null
//    var finishLoginUri: String? = null
}



