package com.cloverj.livepoll.configuration

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ResourceLoader

@Configuration
class MiscConfig {

	@Bean
	public fun objectMapper(): ObjectMapper =
			ObjectMapper()
				.registerModule(KotlinModule())
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

	@Bean
	public fun editorPageContent(resourceLoader: ResourceLoader) : String =
			IOUtils.toString(resourceLoader.getResource("classpath:/app/index.html").inputStream, "UTF-8")


}