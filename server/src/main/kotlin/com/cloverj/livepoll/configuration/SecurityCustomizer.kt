package com.cloverj.livepoll.configuration

import com.disney.studios.commons.singlesignon.SingleSignOnProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.access.AccessDeniedHandlerImpl

/**
 * Customizing the security profile
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class SecurityCustomizer : WebSecurityConfigurerAdapter() {

	@Autowired
	private val props: SingleSignOnProperties? = null

	@Throws(Exception::class)
	override fun configure(http: HttpSecurity) {
		val handler = AccessDeniedHandlerImpl()
		handler.setErrorPage(props!!.web.unauthorizedPath)

		http.exceptionHandling().accessDeniedHandler(handler)
		http.csrf().disable()
		http.headers().frameOptions().deny()
		//http.csrf().ignoringAntMatchers(props.web.completeLoginPath, props.web.completeLogoutPath);
		//http.authorizeRequests().anyRequest().authenticated();
	}


}
