package com.cloverj.livepoll.configuration

import com.cloverj.livepoll.api.domain.Choice
import com.cloverj.livepoll.util.MiscUtil
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ResourceLoader
import java.io.IOException

/**
 * All constant data that is loaded from configuration files
 */
@Configuration
class ConstantsConfiguration(
		val miscUtil: MiscUtil,
		val appProperties: AppProperties
){

//	@Bean
//	@Throws(IOException::class)
//	fun choices(objectMapper: ObjectMapper, resourceLoader: ResourceLoader): List<Choice> =
//			miscUtil.loadObjectFromJsonFile(objectMapper, resourceLoader, appProperties.choicesPath!!, object : TypeReference<List<Choice>>(){})

}
