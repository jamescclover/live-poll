package com.cloverj.livepoll.util

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Service
import java.io.IOException
import java.time.Instant

@Service
class MiscUtil @Autowired constructor(
		private val objectMapper: ObjectMapper){

	val CHARSET = "UTF-8"

	fun now() =
			Instant.now().toEpochMilli();

	fun logObj(obj: Any) = objectMapper.writeValueAsString(obj);

	fun caseInsensitiveRegex(text: String?) =
			if(text != null) ".*(?i)${StringUtils.trimToEmpty(text)}.*"
			else ".*"

	fun everythingPartialRegex() = "(.*)"

	/**
	 * Loads an object with the given type from the given file containing JSON data
	 * @param path the path to the JSON file
	 * @param typeReference the type reference of the returned type, for JSON deserialization
	 * @param <T> the type of the data being returned
	 * @return the data, converted from JSON into the intended type
	 * @throws IOException if the file can't be opened or deserialization fails
	 */
	@Throws(IOException::class)
	fun <T> loadObjectFromJsonFile(objectMapper: ObjectMapper, resourceLoader: ResourceLoader, path: String, typeReference: TypeReference<T>): T {
		val resource = resourceLoader.getResource(path)
		val departmentJson = IOUtils.toString(resource.inputStream, CHARSET)
		return objectMapper.readValue<T>(departmentJson, typeReference)
	}

}