package com.cloverj.livepoll.util

import me.ntrrgc.tsGenerator.TypeScriptGenerator
import java.time.LocalDate
import java.time.LocalDateTime

fun main(args: Array<String>) {
    println(TypeScriptGenerator(
            rootClasses = setOf(

				),
            mappings = mapOf(
                    LocalDateTime::class to "Date",
                    LocalDate::class to "Date"
            )
    ).definitionsText)
}
