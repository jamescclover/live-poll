package com.cloverj.livepoll.feature.security

import com.disney.studios.commons.singlesignon.UserIdentity
import org.apache.commons.lang3.StringUtils
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class SecurityService {

    val ADMIN = "ADMIN"

    private fun auth() : Authentication =
          SecurityContextHolder.getContext().authentication

    fun currentUser() : UserIdentity =
            auth().details as UserIdentity

    fun isCurrentUserAdmin() : Boolean =
          auth().authorities.any { StringUtils.equals(it.toString().toUpperCase(), ADMIN) }

}