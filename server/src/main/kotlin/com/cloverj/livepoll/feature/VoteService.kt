package com.cloverj.livepoll.feature

import com.cloverj.livepoll.api.domain.Choice
import com.cloverj.livepoll.api.domain.ChoicesAndTotals
import com.cloverj.livepoll.api.domain.VoteTotal
import com.cloverj.livepoll.api.exception.ChoiceNotFoundException
import com.cloverj.livepoll.api.exception.PollNotFoundException
import com.cloverj.livepoll.configuration.AppProperties
import com.cloverj.livepoll.feature.security.SecurityService
import com.cloverj.livepoll.util.MiscUtil
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.core.type.TypeReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.core.io.ResourceLoader
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import java.lang.String.format

/**
 * Functions for handling the votes - current vote totals, voting for a choice, clearing all votes
 */
@Service
class VoteService @Autowired constructor(
		val securityService: SecurityService,
		val appProperties: AppProperties,
		val miscUtil: MiscUtil,
		val resourceLoader: ResourceLoader,
		val objectMapper: ObjectMapper
) {
	final val log: Logger = LoggerFactory.getLogger(VoteService::class.java)

	/** The polls that are available with each choice that is available */
	val polls: Map<String, List<Choice>> = appProperties.pollConfigurations.associateBy(
			{it},
			{
				miscUtil.loadObjectFromJsonFile(
						objectMapper, resourceLoader,
						nameToResourcePath(it),
						object : TypeReference<List<Choice>>(){})
			})

	/** The current set of votes (yeah, pretty simple, don't use for anything "real") */
	val votes: Map<String, MutableMap<String, Int>> = appProperties.pollConfigurations.associateBy(
			{it},
			{ mutableMapOf<String, Int>()} )


	/**
	 * Returns the IDs for all polls
	 */
	fun currentPollIds() =
			polls.keys.toSortedSet()

	/**
	 * Returns the current state of the possible choices and, if the user is an admin, returns the vote totals as well
	 */
	fun currentState(pollId: String) : ChoicesAndTotals =
			ChoicesAndTotals(
					pollId(pollId),
					securityService.isCurrentUserAdmin(),
					choices(pollId),
					calcTotals(pollId),
					null)

	/**
	 * Sets all the vote counts to zero for the given poll - only can be used if the user is an admin
	 */
	@PreAuthorize("@securityService.isCurrentUserAdmin()")
	fun resetTotals(pollId: String) : ChoicesAndTotals {
		synchronized(votes) {
			votes(pollId).clear()
		}

		return currentState(pollId)
	}

	/**
	 * Registers a single vote for the given choice in the given poll
	 */
	fun voteFor(pollId: String, choiceId: String) : ChoicesAndTotals {
		synchronized(votes) {
			votes(pollId).put(choiceId, (votes(pollId)[choiceId] ?: 0) + 1)
		}

		return currentState(pollId);
	}

	/**
	 * Validates that the given poll exists and returns its ID if it does
	 */
	private fun pollId(pollId: String): String =
			if(polls[pollId] != null) pollId else throw PollNotFoundException()

	/**
	 * Returns the map with the votes for the given poll id, or throws an error if it doesn't exist
	 */
	private fun votes(pollId: String): MutableMap<String, Int> =
			votes[pollId] ?: throw PollNotFoundException()

	/**
	 * Returns the choices in the given poll
	 */
	private fun choices(pollId: String): List<Choice> =
			polls[pollId] ?: throw PollNotFoundException()

	/**
	 * Returns the choice with the given id in the given poll, or throws an error if it doesn't exist
	 */
	private fun choice(pollId: String, choiceId: String): Choice =
			choices(pollId).find { it.id == choiceId } ?: throw ChoiceNotFoundException()

	/**
	 * Returns the ID of the choice with the given ID in the given poll, or throws an error if it doesn't exist.
	 * We use this method for the error checking that finding the choice and returning its ID gives us, even though
	 * it should always return the ID that was passed in
	 */
	private fun choiceId(pollId: String, choiceId: String): String =
			choice(pollId, choiceId).id

	/**
	 * Calculates the totals of all votes
	 */
	private fun calcTotals(pollId: String): List<VoteTotal>  =
			votes(pollId).entries.map { VoteTotal(it.key, it.value) }

	private fun nameToResourcePath(name: String) =
			format("classpath:/%s.json", name)
}