package com.cloverj.livepoll

import com.cloverj.livepoll.configuration.AppProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(AppProperties::class)
class LivePollApplication

fun main(args: Array<String>) {
	runApplication<LivePollApplication>(*args)
}

