package com.cloverj.livepoll.api.web

import com.cloverj.livepoll.api.domain.ChoicesAndTotals
import com.cloverj.livepoll.api.domain.UserVoteChoice
import com.cloverj.livepoll.feature.VoteService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.lang.String.format
import java.time.Instant
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping(path=["/api/polls"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
class ApiController @Autowired constructor(
      private val voteService: VoteService
) {
    private val log: Logger = LoggerFactory.getLogger(javaClass)
    private val COOKIE_NAME = "live-poll-vote"
    private var lastResetVoteTimestamp = Instant.now()

    @GetMapping()
    fun pollList() =
          voteService.currentPollIds()

    @GetMapping("/{pollId}/current")
    fun currentState(@PathVariable pollId: String, request: HttpServletRequest, response: HttpServletResponse) : ChoicesAndTotals =
        voteService.currentState(pollId).setCurrentChoice(getOrClearVoteChoice(pollId, request, response))

    @PostMapping("/{pollId}/reset")
    fun resetTotals(@PathVariable pollId: String) : ChoicesAndTotals {
        this.lastResetVoteTimestamp = Instant.now()
        return voteService.resetTotals(pollId)
    }

    @PutMapping("/{pollId}/vote-for/{choiceId}")
    fun voteFor(@PathVariable pollId: String, @PathVariable choiceId: String, response: HttpServletResponse) : ChoicesAndTotals {
        val choicesAndTotals = voteService.voteFor(pollId, choiceId)
        choicesAndTotals.voteChoice = choiceId
        setVoteChoice(pollId, choiceId, response)
        return choicesAndTotals;
    }

    private fun setVoteChoice(pollId: String, choiceId: String, response: HttpServletResponse) {
        val idAndDate = format("%s-%s-%d", pollId, choiceId, Instant.now().toEpochMilli())
        val cookie = Cookie(COOKIE_NAME, idAndDate)
        cookie.path = "/"
        response.addCookie(cookie)
    }

    private fun getOrClearVoteChoice(pollId: String, request: HttpServletRequest, response: HttpServletResponse) : String? {
        try {
            val cookieValue = request.cookies.find { it.name == COOKIE_NAME }?.value
            if (cookieValue != null) {
                val usersVote = UserVoteChoice(cookieValue)

                if(usersVote.pollId != pollId) {
                    return null
                }
                else if(Instant.ofEpochMilli(usersVote.timestamp).isBefore(lastResetVoteTimestamp)) {
                    deleteVoteChoice(response)
                    return null
                }
                else {
                    return usersVote.choice
                }
            }
            else {
                return null
            }
        }
        catch (t: Throwable) {
            log.error("Error getting cookie", t)
            deleteVoteChoice(response)
            return null
        }
    }

    /**
     * Delete the vote tracking cookie
     */
    fun deleteVoteChoice(response: HttpServletResponse) {
        val cookie = Cookie(COOKIE_NAME, null)
        cookie.path = "/"
        cookie.maxAge = 0
        response.addCookie(cookie)
    }

}
