package com.cloverj.livepoll.api.domain

class VoteTotal(
	val id: String,
	val total: Int
)
