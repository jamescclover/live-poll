package com.cloverj.livepoll.api.domain

class Choice (
	val id: String,
	val name: String,
	val imageUrl: String
)
