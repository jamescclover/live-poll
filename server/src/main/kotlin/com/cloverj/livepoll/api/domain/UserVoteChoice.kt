package com.cloverj.livepoll.api.domain

import org.apache.commons.lang3.math.NumberUtils

/**
 * Holds the value of the cookie that represents a user's vote
 */
class UserVoteChoice(encodedValue: String) {
	val pollId: String
	val choice: String
	val timestamp: Long

	init {
		val splitValue = encodedValue.split("-")
		pollId = splitValue[0]
		choice = splitValue[1]
		timestamp = NumberUtils.toLong(splitValue[2])
	}
}