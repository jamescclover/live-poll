package com.cloverj.livepoll.api.domain

class ChoicesAndTotals(
		val pollId: String,
		val isAdmin: Boolean,
		val choices: List<Choice>,
		val totals: List<VoteTotal>?,
		var voteChoice: String? = null
)  {

	/**
	 * Convenience method to set the vote choice and return the updated object
	 */
	fun setCurrentChoice(id: String?) : ChoicesAndTotals {
		this.voteChoice = id;
		return this;
	}

}