package com.cloverj.livepoll.api.web

import com.cloverj.livepoll.configuration.AppProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping(produces = [MediaType.TEXT_HTML_VALUE])
class HomeController @Autowired constructor(
        private val editorPageContent: String,
        private val props: AppProperties

) {
    private val log: Logger = LoggerFactory.getLogger(javaClass)

    @GetMapping()
    @ResponseBody
    fun default() : String =
          editorPageContent

    @GetMapping("/home")
    fun home() : String =
          "redirect:${props.security?.baseUri}/"


}
