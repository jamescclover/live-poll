package com.cloverj.livepoll

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.PropertySources
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import retrofit2.Call
import spock.lang.Specification

/**
 * Configuration for tests using a Spring context
 */
@ContextConfiguration
@PropertySources([@PropertySource("classpath:application.yaml")])
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
abstract class AbstractIntegrationTest extends Specification {

   // @Autowired LoginHelperFactory loginHelperFactory

    /**
     * Executes the given call, logging an exception and throwing an error if the call fails
     */
    protected <T> T runAndBody(Call<T> call, HttpStatus expectedStatus = HttpStatus.OK) {
        retrofit2.Response<?> response = call.execute();
        if(expectedStatus.is2xxSuccessful() && response.isSuccessful()) {
            return response.body()
        }
        else {
            String message = String.format("Call %s failed with %s; body:\n%s", call.request(), response.code(), response.body());
            throw new CallFailedException(message);
        }
    }

    /**
     * Alias for runAndBody
     */
    protected <T> T rb(Call<T> call, HttpStatus expectedStatus = HttpStatus.OK) {
        return runAndBody(call, expectedStatus)
    }

    /**
     * Cleans up all test data, leaving only the built-in data
     */
    protected cleanUpTestData() {
    }


}
