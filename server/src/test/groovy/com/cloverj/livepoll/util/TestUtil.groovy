package com.cloverj.livepoll.util

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

/**
 * Some testing utilities
 */
class TestUtil {

    static public buildObjectMapper() {
        return new ObjectMapper()
                .registerModule(new KotlinModule())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }
}