package com.cloverj.livepoll
/**
 * Thrown when an call fails
 */
class CallFailedException extends RuntimeException {
    CallFailedException() {
    }

    CallFailedException(String var1) {
        super(var1)
    }

    CallFailedException(String var1, Throwable var2) {
        super(var1, var2)
    }

    CallFailedException(Throwable var1) {
        super(var1)
    }

    CallFailedException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4)
    }
}